import React, { Component } from 'react';
import UserInput from './UserInput/UserInput'
import UserOutput from './UserOutput/UserOutput'

class App extends Component {
    state = {
       username:"Shortness"
    }
    switchNameHandler = (event) =>
    {
        this.setState({username:event.target.value});
    }
    render() {
        const style = {
            backgroundColor:'black',
            font:'inherit',
            border: '5px solid blue',
            padding:'81px',
        }
        return (
            <div>
                <UserInput
                    username={this.state.username}
                    changed={this.switchNameHandler}
                />
                <UserOutput
                    style={style}
                    username={this.state.username}/>
                <UserOutput/>
                <UserOutput/>
            </div>
        );
    }
}

export default App;
